package cn.dota2info.mq.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;


import java.util.Date;


@TableName("transaction_log")
public class TransactionLog{
    private static final long serialVersionUID = 5199200306752426433L;


    @TableId(value="id", type= IdType.AUTO)
    private Integer id;

    @TableField("version")
    private String  version;

    @TableField("topic")
    private String  topic;

    @TableField("tag")
    private String  tag;

    @TableField("key")
    private String  key;

    @TableField("status")
    private Integer  status;

    @TableField("create_time")
    private Date create_time;

    @TableField("update_time")
    private Date update_time;



}
